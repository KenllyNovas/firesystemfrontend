import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Categoria from './components/Bombero.vue'
import Articulo from './components/Incendio.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/bomberos',
      name: 'bomberos',
      component: Categoria
    },
    {
      path: '/incendios',
      name: 'incendios',
      component: Articulo
    }
  ]
})
